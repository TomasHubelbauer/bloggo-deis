# Deis

Similar to Flynn ([GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-flynn), [Bloggo post](http://hubelbauer.net/post/flynn)).

Deis refers to Deis Workflow, the new version of the offering built on Kubernetes.
The initial version built on CoreOS is no longer maintained.

- [ ] Compare the two solutions and note down the differences

## Links

- [GitHub repository](https://github.com/deis/workflow)
